ARG nuget_version="v6.3.0"
ARG ms_build_version="17"
ARG vs_version="2022"

FROM mcr.microsoft.com/windows/servercore:ltsc2022

LABEL maintainer=compulim@hotmail.com description="MSBuild ${vs_version}"

ADD https://aka.ms/vs/${ms_build_version}/release/vs_BuildTools.exe C:\\Downloads\\vs_buildtools.exe
ADD https://dist.nuget.org/win-x86-commandline/${nuget_version}/nuget.exe C:\\Nuget\\nuget.exe

RUN C:\\Downloads\\vs_buildtools.exe --add Microsoft.VisualStudio.Workload.MSBuildTools --add Microsoft.VisualStudio.Workload.NetCoreBuildTools --add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Workload.WebBuildTools --quiet --wait
RUN SETX /M Path "%Path%;C:\\Nuget;C:\\Program Files (x86)\\Microsoft Visual Studio\\${vs_version}\\BuildTools\\MSBuild\\${ms_build_version}.0\\Bin"
